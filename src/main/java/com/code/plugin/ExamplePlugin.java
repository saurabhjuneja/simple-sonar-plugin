package com.code.plugin;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.Properties;
import org.sonar.api.Property;
import org.sonar.api.SonarPlugin;

import com.code.sensor.ExampleSensor;
import com.code.widget.ExampleFooter;
import com.code.widget.ExampleRubyWidget;

/** This class is the entry point for all extensions */
@Properties({ @Property(key = ExamplePlugin.USER_STORY, name = "User Story", description = "User Story Number", defaultValue = "US-1234") })
public final class ExamplePlugin extends SonarPlugin {

    public static final String USER_STORY = "sonar.code.userstory";

    // This is where you're going to declare all your SonarQube extensions
    @SuppressWarnings("rawtypes")
    @Override
    public List getExtensions() {
        return Arrays.asList(
        // Definitions
                ExampleMetrics.class, 

                // Batch
                ExampleSensor.class,

                // UI
                ExampleFooter.class, ExampleRubyWidget.class);
    }
}
