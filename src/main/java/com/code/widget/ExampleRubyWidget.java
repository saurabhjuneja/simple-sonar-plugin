package com.code.widget;

import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.Description;
import org.sonar.api.web.RubyRailsWidget;
import org.sonar.api.web.UserRole;
import org.sonar.api.web.WidgetCategory;

@UserRole(UserRole.USER)
@Description("Calculates and Show User Story Specific Value")
@WidgetCategory("Sample")
public class ExampleRubyWidget extends AbstractRubyTemplate implements RubyRailsWidget {

    @Override
    public String getId() {
        return "sample";
    }

    @Override
    public String getTitle() {
        return "USSV Tool";
    }

    @Override
    protected String getTemplatePath() {
        return "/example/example_widget.html.erb";
    }

}
