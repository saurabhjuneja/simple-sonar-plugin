package com.code.sensor;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.Sensor;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.PropertiesBuilder;
import org.sonar.api.resources.Project;

import com.code.plugin.ExampleMetrics;
import com.code.plugin.ExamplePlugin;

public class ExampleSensor implements Sensor {

	private static final Logger LOG = LoggerFactory
			.getLogger(ExampleSensor.class);

	private Settings settings;
	PropertiesBuilder<String, String> propertBuilderValue = new PropertiesBuilder<String, String>();

	/**
	 * Use of IoC to get Settings and FileSystem
	 */
	public ExampleSensor(Settings settings) {
		this.settings = settings;
	}

	@Override
	public boolean shouldExecuteOnProject(Project project) {
		// This sensor is executed only when there are Java files
		return true;
	}

	@Override
	public void analyse(Project project, SensorContext sensorContext) {
		// This sensor create a measure for metric MESSAGE on each Java file
		String getDirec = settings.getString("sonar.projectBaseDir");
		String valueFor = settings.getString("sonar.scm.provider");
		String userStory = settings.getString(ExamplePlugin.USER_STORY);
		String os = settings.getString("sun.desktop");
		LOG.info("Value for User Stroy = " + userStory);
		LOG.info("Value Project Type =" + valueFor);
		LOG.info("Value Project Path =" + getDirec);
		LOG.info("os =" + os);
		if (valueFor == null) {
			File f = new File(getDirec + "/.git");
			if (f.exists()) {
				valueFor = "git";
			} else {
				f = new File(getDirec + "/.svn");
				if (f.exists()) {
					valueFor = "svn";
				}
			}
		}
		LOG.info("Value for Project Type =" + valueFor);
		try {

			String[] userStories = { userStory };
	        int count = 0;
	        if (userStory != null && userStory.contains(",")) {
	            userStories = userStory.split(",");
	        }
	        Map<String, String> resultValueMap = new HashMap<String, String>();
	        
	        while (userStories.length > count) {
	        	resultValueMap.put(userStories[count], Math.round(((count + Math.random()*100)%100))*100/100.00+"");
	        	count++;
	        }
			
			if (userStory == null)
				LOG.info("User Stroy is null");
			if (getDirec == null)
				LOG.info("Project Path is null");
			for (Map.Entry<String, String> a : resultValueMap.entrySet()) {
				propertBuilderValue.add(a.getKey(), a.getValue());
			}
		} catch (Exception e) {
		    LOG.error("error", e);
		}
		LOG.info("propertBuilderValue =" + propertBuilderValue);
		LOG.info("propertBuilderValue.buildData() =" + propertBuilderValue.buildData());
		sensorContext.saveMeasure(new Measure(
				ExampleMetrics.RESULT_VALUE_MAP, propertBuilderValue.buildData()));
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
